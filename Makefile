# -- Development --------------------------------------------------------------
develop:
	pip3 install --no-binary=:all: -r requirements-dev.txt

migrate:
	python3 manage.py migrate --database=default
	python3 manage.py migrate --database=transient


# -- Translations -------------------------------------------------------------
collect_translations:
	python3 manage.py makemessages --all --no-obsolete --ignore=debian --ignore=storage -d django

push_translations:
	tx push -s

pull_translations:
	tx pull --all --force

compile_translations:
	python3 manage.py compilemessages


# -- Testing ------------------------------------------------------------------
test:
	py.test

test-coverage:
	py.test --cov=irfi

quality-check:
	py.test --flakes -m flakes

check-missing-migrations:
	python manage.py makemigrations
	git status --porcelain | grep -E '^\?\? ' && exit 1 || :
