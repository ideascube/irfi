from django.conf.urls import i18n, url

from ideascube.urls import urlpatterns
from django.views.generic import TemplateView

from . import views


urlpatterns += i18n.i18n_patterns(
    url(r'^rfi/message/new/$', views.message_create, name='message_create'),
    url(r'^rfi/message/thank/$',
        TemplateView.as_view(template_name='irfi/message_thank.html'),
        name='message_thank'),

    url(r'^rfi/radio/$', views.radio_index, name='radio_index'),
    url(r'^rfi/radio/new/$', views.radio_create, name='radio_create'),
    url(r'^rfi/radio/(?P<pk>[\d]+)/$', views.radio_detail,
        name='radio_detail'),
    url(r'^rfi/radio/(?P<pk>[\d]+)/edit/$', views.radio_update,
        name='radio_update'),
    url(r'^rfi/radio/(?P<pk>[\d]+)/delete/$', views.radio_delete,
        name='radio_delete'),
)
