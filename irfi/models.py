from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ideascube.blog.models import Content, ContentQuerySet
from ideascube.mediacenter.models import Document, DocumentQuerySet


class ListenerMessage(Content):
    media = models.ForeignKey(Document, blank=True, null=True)
    objects = ContentQuerySet.as_manager()


class RadioShow(Document):
    THEMES = (
        ('healthcare', _('Access to health care')),
        ('education', _('Access and quality of education')),
        ('environment', _('Respect and protection of the environment, ecotourism')),
        ('agriculture', _('Production and agricultural consumption')),
        ('water_energy', _('Access and management of water and energy')),
        ('women_rights', _("Women's rights")),
        ('civil_rights', _('Participatory management and civil rights')),
        ('youth', _('Youth and perspectives')),
        ('culture', _('Arts, culture and heritage')),
        ('migration', _('Migration and integration')),
        ('media_local', _('Impact of new media on local communication')),
        ('misc', _('Miscellaneous'))
    )

    air_date = models.DateField(verbose_name=_('air date'))
    theme = models.CharField(
        verbose_name=_('theme'), max_length=255, choices=THEMES)

    objects = DocumentQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse('radio_detail', kwargs={'pk': self.pk})

    @property
    def size(self):
       _size = self.original.size
       units = [(_("GiB"), 1024**3),
                (_("MiB"), 1024**2),
                (_("KiB"), 1024**1),
                (_("B"), 1)]
       for unity, divider in units:
           if _size > divider:
               return "{} {}".format(_size//divider, unity)
       return "{} {}".format(_size, _("B"))

