from datetime import datetime, timedelta

from bs4 import BeautifulSoup

from django.core.management.base import BaseCommand

from irfi.models import ListenerMessage, RadioShow


class Command(BaseCommand):
    help = 'Remove old radio shows and blog articles.'

    def add_arguments(self, parser):
        parser.add_argument('older_than', type=int,
            help='Clean content older than this many months.')

    def handle(self, *args, **options):
        today = datetime.today()

        # timedelta takes a number of days; 30 days per month is a good-enough
        # approximation.
        num_days = options['older_than'] * 30
        max_age = today - timedelta(days=num_days)

        old_shows = RadioShow.objects.filter(air_date__lt=max_age)
        old_shows.delete()

        old_messages = ListenerMessage.objects.filter(published_at__lt=max_age)

        for message in old_messages:
            if message.media:
                message.media.delete()

            message.delete()
