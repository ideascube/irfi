# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-25 14:13
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('irfi', '0002_auto_20180119_1617'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='radioshow',
            name='duration',
        ),
    ]
