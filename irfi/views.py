from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.db import IntegrityError
from django.db.models import F
from django.db.models.functions import Lower
from django.shortcuts import render
from django.utils import timezone
from django.utils.translation import get_language, ugettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    FormView,
    ListView,
    UpdateView
)

from ideascube.configuration import set_config
from ideascube.decorators import staff_member_required
from ideascube.models import User

from ideascube.blog.models import Content
from ideascube.mediacenter.forms import DocumentForm
from ideascube.mediacenter.models import Document
from ideascube.mixins import FilterableViewMixin, OrderableViewMixin

from .forms import ContentForm, MessageForm, RadioForm
from .models import RadioShow


class MessageCreate(FormView):
    form_class = MessageForm
    template_name = 'irfi/message_form.html'

    @staticmethod
    def _generate_message_text(text, document):
        media_text = ''
        if document:
            media_text = (
                '<{type} controls preload="none" width="100%">\n'
                '  <source src="{url}" data-irfi-document="{id}">\n'
                '</{type}>').format(
                    type=document.kind, url=document.original.url,
                    id=document.id)

        message = '{media}\n{text}'.format(
            media=media_text,
            text=text)

        return message

    def get_success_url(self):
        return reverse('message_thank')

    def form_valid(self, form):
        # Some variables
        author = form.cleaned_data['author']
        media = form.cleaned_data['media']

        # FIXME: We can't make those translatable in the time we have;
        # Let's hardcode in French.
        if form.cleaned_data['kind'] == 'advertisement':
            title = "Annonce de {}".format(author)
        else:
            title = "Message de {}".format(author)

        # Store user information in a cookie
        user_serial = base_user_serial = 'listener_{}'.format(author)[:40]
        user_serial_id = 0
        while True:
            try:
                user = User.objects.create(
                    serial=user_serial,
                    full_name=author,
                    birth_year=form.cleaned_data['birth_year'],
                    # This is a nasty hack, we store the contact that can be
                    # something else than a phone number in the phone field.
                    phone=form.cleaned_data['contact'],
                    camp_address=form.cleaned_data['address'],
                    gender=form.cleaned_data['gender'])
                break
            except IntegrityError:
                user_serial_id += 1
                extension = "~{}".format(user_serial_id)
                user_serial = "{}{}".format(
                    base_user_serial[:40-len(extension)],
                    extension)

        # Create the document
        document = None
        if media:
            # Create the document
            files = dict(original=media, preview=None)
            document_metadata = {
                'title': title,
                'hidden': True,
                'kind': Document.OTHER
            }
            document_form = DocumentForm(data=document_metadata, files=files)
            document = document_form.save()

        # Create the blog object
        blog_metadata = {
            'title': title,
            'author': user.pk,
            'published_at': timezone.now(),
            'status': Content.DRAFT,
            'lang': get_language(),
            'media': document.id if document else None,
            'text': self._generate_message_text(
                text=form.cleaned_data['text'],
                document=document
            )
        }
        blog_form = ContentForm(data=blog_metadata)
        blog = blog_form.save()

        return super().form_valid(form)


class RadioQuerysetMixin:
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(hidden=True)
        qs = qs.search(tags=['radio'])

        return qs


class RadioIndex(
        RadioQuerysetMixin, FilterableViewMixin, OrderableViewMixin, ListView):

    ORDERS = [
        {
            'key': 'air_date',
            'label': _('Date'),
            'expression': F('air_date'),
            'sort': 'desc'
        },
        {
            'key': 'title',
            # Translators: Do not translate this, the translation is in Ideascube
            'label': _('Title'),
            'expression': Lower('title'),  # Case insensitive.
            'sort': 'asc'
        }
    ]

    model = RadioShow
    paginate_by = 24

    def _set_available_kinds(self, context):
        # This is all audio files
        context['available_kinds'] = []

    def _set_available_tags(self, context):
        # All the files are tagged as "radio" only, and we don't display the
        # tag cloud in the UI
        context['available_tags'] = []

    def _set_available_themes(self, context):
        # We can't use self.queryset() here, as that would filter on the
        # query string
        qs = RadioShow.objects.all()
        qs = qs.filter(hidden=True)
        qs = qs.search(tags=['radio'])

        theme_slugs = qs.values_list('theme', flat=True).distinct()

        context['available_themes'] = [
            (slug, label) for (slug, label) in RadioShow.THEMES
            if slug in theme_slugs
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # This tells the search system to only look at hidden documents
        context['public'] = False

        self._set_available_kinds(context)
        self._set_available_langs(context)
        self._set_available_tags(context)
        self._set_available_themes(context)

        theme = self.request.GET.get('theme')
        for t in RadioShow.THEMES:
            if t[0] == theme:
                context['theme'] = theme
                context['theme_name'] = t[1]

        return context

    def get_queryset(self):
        qs = super().get_queryset()

        # The theme is added in irfi, as such it is not indexed by the
        # Ideascube search system
        theme = self.request.GET.get('theme')

        if theme is not None:
            qs = qs.filter(theme=theme)

        return qs


class RadioDetail(RadioQuerysetMixin, DetailView):
    model = RadioShow


class RadioCreate(CreateView):
    model = RadioShow
    form_class = RadioForm
    initial = {
        'hidden': True,
        'lang': get_language(),
    }


class RadioUpdate(RadioQuerysetMixin, UpdateView):
    model = RadioShow
    form_class = RadioForm


class RadioDelete(RadioQuerysetMixin, DeleteView):
    model = RadioShow
    success_url = reverse_lazy('radio_index')


message_create = MessageCreate.as_view()

radio_index = RadioIndex.as_view()
radio_detail = RadioDetail.as_view()
radio_create = staff_member_required(RadioCreate.as_view())
radio_delete = staff_member_required(RadioDelete.as_view())
radio_update = staff_member_required(RadioUpdate.as_view())
