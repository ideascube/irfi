from datetime import datetime
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from ideascube.models import User
from ideascube.mediacenter import utils
from ideascube.mediacenter.forms import DocumentForm
from ideascube.mediacenter.models import Document
from ideascube.widgets import LangSelect, RichTextEntry

from .models import ListenerMessage, RadioShow


current_year = datetime.now().year


# We need to redefine all this
class ContentForm(forms.ModelForm):
    class Meta:
        model = ListenerMessage
        widgets = {
            "author": forms.HiddenInput,
            "published_at": forms.DateInput(format='%d-%m-%Y'),
            "lang": LangSelect,
            "summary": RichTextEntry,
            "text": RichTextEntry(with_media=True)
        }
        fields = "__all__"

    def save(self, commit=True):
        content = super().save()
        content.save()  # Index m2m.

        return content


class MessageForm(forms.Form):
    # Translators: Do not translate this, the translation is in Ideascube
    author = forms.CharField(label=_("First name and last name"), max_length=100)
    # Translators: Do not translate this, the translation is in Ideascube
    birth_year = forms.IntegerField(label=_("Birth year"), required=True,
         min_value=current_year-150, max_value=current_year,
         localize=True)
    address = forms.CharField(label=_("Locality"), widget=forms.Textarea,
        required=False)
    contact = forms.CharField(label=_("Contact (Phone or email)"), required=False)
    gender = forms.ChoiceField(label=_("Gender"), required=False,
                               choices=User.GENDER_CHOICES)
    text = forms.CharField(label=_("Text"), widget=RichTextEntry,
        required=False)
    media = forms.FileField(
        label=_("Audio or video file"), required=False,
        widget=forms.ClearableFileInput(
            attrs={
                'accept': (
                    '.mp3,.mp4,.wav, '
                    'audio/mpeg,audio/mp3,audio/mp4,audio/wav,video/mp4'
                ),
            }
        ))
    kind = forms.ChoiceField(label=_("Object"),
                             choices=[('advertisement',
                                           _('I want to publish a communiqué')),
                                      ('message',
                                           _('I want to leave a message'))])

    def clean(self):
        cleaned_data = super().clean()
        media = cleaned_data['media']
        if not media and not cleaned_data['text']:
            raise forms.ValidationError(
                _("You must at least send a media or enter a text"))

        if media:
            document_kind = utils.guess_kind_from_content_type(
                media.content_type)
            if not document_kind:
                document_kind = utils.guess_kind_from_extension(media.name)

            if document_kind not in (Document.AUDIO, Document.VIDEO):
                self.add_error(
                    'media', ValidationError(
                                 _('Only audio or video files are allowed'))
                )

        return cleaned_data


class RadioForm(DocumentForm):
    class Meta:
        model = RadioShow
        fields = '__all__'
        widgets = {
            'lang': LangSelect,
            'summary': RichTextEntry,
            'air_date': forms.DateInput(format='%Y-%m-%d'),
            'original': forms.ClearableFileInput(attrs={
                'accept': '.mp3,.wav, audio/mpeg,audio/mp3,audio/wav'
            }),

            # Those won't be changed by users
            'hidden': forms.HiddenInput,
            'kind': forms.HiddenInput,
            'tags': forms.HiddenInput,
        }
        exclude = [
            'credits',
            'package_id',
            'preview',
        ]

    def clean_tags(self):
        # We absolutely want the "radio" tag here, because that's how we
        # distinguish radio shows from usual medias.
        # We do not want any other tag though, since they aren't displayed
        # anywhere.
        return ['radio']

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data['kind'] != RadioShow.AUDIO:
            self.add_error(
                'original', ValidationError(_('Only audio files are allowed')))
            return

        # TODO: preview based on radio name and theme

        return cleaned_data
