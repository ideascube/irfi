import os

from setuptools import setup, find_packages

import ideascube


here = os.path.abspath(os.path.dirname(__file__))


with open(os.path.join(here, 'README.rst'), 'r') as f:
    long_description = f.read()


setup(
    name='irfi',
    version=ideascube.__version__,
    description=('This Django repository is the main application of the Ideas '
                 'Box server.'),
    long_description=long_description,
    url='https://framagit.org/irfi/irfi/',
    author='BSF IT Team',
    author_email='it@bibliosansfrontieres.org',
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        ('License :: OSI Approved :: GNU Affero General Public License v3 or '
         'later (AGPLv3+)'),
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
    ],
    packages=find_packages(),
    include_package_data=True,
)
